import { db, auth } from "../firebase/Setting";

export const CreateUser = (PUSH) => {
 let data = {
  username: document.getElementById("signUpUserName").value,
  name: document.getElementById("signUpName").value,
  lastName: document.getElementById("signUpLastName").value,
  email: document.getElementById("signUpEmail").value,
  password: document.getElementById("signUpPassword").value,
 };
 auth
  .createUserWithEmailAndPassword(data.email, data.password)
  .then((userCredential) => {
   db.collection("users").doc().set(data);
   console.log("Exito");
   PUSH();
  })
  .catch((error) => {
   console.log(error);
  });
};

export const UpdateUser = (props) => {
 let data = {
  username: props.username,
  name: props.name,
  lastName: props.lastName,
  email: props.email,
  password: props.password,
 };
 db.collection("users").doc("wllXFUN8CCcZp7kjyqxU").set(data);
 console.log("Actualizacion exitosa");
};

export const DeleteUser = (props) => {
 db.collection("users").doc(props).delete();
 auth.currentUser
  .delete()
  .then(() => {
   console.log("Usuario eliminado exitosamente");
  })
  .catch((error) => {
   console.log(error);
  });
};
