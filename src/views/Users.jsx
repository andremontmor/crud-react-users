import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { db } from "../firebase/Setting";
import { DeleteUser } from "../data/CRUD";
import { getUser, hookSetStateAuth } from "../firebase/Authentication";
import { List, ListItem, ListItemSecondaryAction, ListItemText, ListItemAvatar, Avatar, IconButton, Box } from "@material-ui/core";
import { Delete, Edit } from "@material-ui/icons";
import useStyles from "../styles/css";

const Users = (props) => {
 const css = useStyles();
 const history = useHistory();
 const [users, setUsers] = useState([]);
 const [stateAuth, setStateAuth] = useState(false);

 useEffect(() => {
  /* GET USERS */
  db.collection("users").onSnapshot((data) => {
   setUsers(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
  });
  hookSetStateAuth(setStateAuth);
 }, []);

 return (
  <Box display="flex" justifyContent="center" m={3}>
   <List align="right" className={css.listUsers}>
    {users.length > 0 ? (
     users.map((user) => (
      <ListItem key={user.id} button>
       <ListItemAvatar>
        <Avatar />
       </ListItemAvatar>
       <ListItemText id={user.id} primary={user.name} secondary={`@${user.username}`} />

       {stateAuth ? (
        getUser().email === user.email ? (
         <ListItemSecondaryAction>
          <IconButton
           edge="end"
           aria-label="edit"
           style={{ color: "blue" }}
           onClick={() => {
            history.push("UpdateUser");
           }}>
           <Edit />
          </IconButton>
          <IconButton
           edge="end"
           aria-label="delete"
           onClick={() => {
            DeleteUser(user.id);
           }}
           style={{ color: "red" }}>
           <Delete />
          </IconButton>
         </ListItemSecondaryAction>
        ) : null
       ) : null}
      </ListItem>
     ))
    ) : (
     <ListItem>
      <ListItemText id="NotUsers" primary="Not Users" />
     </ListItem>
    )}
   </List>
  </Box>
 );
};
export default Users;
