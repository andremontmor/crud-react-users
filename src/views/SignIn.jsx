import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { Avatar, Button, CssBaseline, TextField, FormControlLabel, Checkbox, Link, Grid, Typography, Container, IconButton } from "@material-ui/core";
import { LockOutlined } from "@material-ui/icons";
import { signIn } from "../firebase/Authentication";
import useStyles from "../styles/css";
import Alert from "../components/Alert";

const SignIn = () => {
 const css = useStyles();
 const history = useHistory();
 const [alertErrorAuth, setAlertErrorAuth] = useState(false);
 const [messageError, setMessageError] = useState("");
 const pushHome = () => {
  history.push("/");
 };

 return (
  <Container component="main" maxWidth="xs">
   <CssBaseline />
   <div className={css.paper}>
    <Avatar className={css.avatar}>
     <LockOutlined />
    </Avatar>
    <Typography component="h1" variant="h5">
     Sign in
    </Typography>
    <form
     className={css.form}
     onSubmit={(e) => {
      e.preventDefault();
      signIn(pushHome, setAlertErrorAuth, setMessageError);
     }}>
     <TextField id="signInEmail" name="email" label="Email Address" autoComplete="email" type="email" margin="normal" variant="outlined" fullWidth required autoFocus />
     {alertErrorAuth ? <Alert state={setAlertErrorAuth} messageError={messageError} /> : null}
     <TextField id="signInPassword" name="password" label="Password" autoComplete="current-password" type="password" margin="normal" variant="outlined" required fullWidth />
     <FormControlLabel control={<Checkbox value="remember" color="primary" />} label="Remember me" />
     <Button className={css.submit} color="primary" type="submit" variant="contained" fullWidth>
      Sign In
     </Button>
     <Grid container>
      <Grid item xs>
       <Link href="#" variant="body2">
        Forgot password?
       </Link>
      </Grid>
      <Grid item>
       <Link href="#" variant="body2">
        Don't have an account? Sign Up
       </Link>
      </Grid>
     </Grid>
    </form>
   </div>
  </Container>
 );
};

export default SignIn;
