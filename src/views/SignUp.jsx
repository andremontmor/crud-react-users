import React from "react";
import { useHistory } from "react-router-dom";
import { CreateUser } from "../data/CRUD";
import { Avatar, Button, CssBaseline, TextField, FormControlLabel, Link, Grid, Typography, Container, Checkbox } from "@material-ui/core";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
/* import { ValidatorForm, TextValidator } from "react-material-ui-form-validator"; */
import useStyles from "../styles/css";
const SignUp = () => {
 const css = useStyles();
 let history = useHistory();
 const PUSH = () => {
  history.push("/");
 };
 /* const data = {
    username: "",
    name: "",
    lastName: "",
    email: "",
    password: "",
  }; */

 const handleSubmit = (e) => {
  e.preventDefault();
  CreateUser(PUSH);
 };

 return (
  <Container component="main" maxWidth="xs">
   <CssBaseline />
   <div className={css.paper}>
    <Avatar className={css.avatar}>
     <LockOutlinedIcon />
    </Avatar>
    <Typography component="h1" variant="h5">
     Sign Up
    </Typography>
    <form className={css.form} onSubmit={handleSubmit}>
     <Grid container spacing={2}>
      <Grid item xs={12}>
       <TextField id="signUpUserName" name="userName" label="User Name" autoComplete="Name" variant="outlined" fullWidth required autoFocus />
      </Grid>
      <Grid item xs={12} sm={6}>
       <TextField id="signUpName" name="firstName" label="First Name" autoComplete="First name" variant="outlined" fullWidth required />
      </Grid>
      <Grid item xs={12} sm={6}>
       <TextField id="signUpLastName" name="lastName" label="Last Name" autoComplete="lname" variant="outlined" required fullWidth />
      </Grid>
      <Grid item xs={12}>
       <TextField id="signUpEmail" name="email" label="Email Address" autoComplete="email" type="email" variant="outlined" fullWidth />
      </Grid>
      <Grid item xs={12}>
       <TextField id="signUpPassword" name="password" label="Password" type="password" variant="outlined" required fullWidth autoComplete="current-password" />
      </Grid>
      <Grid item xs={12}>
       <FormControlLabel control={<Checkbox value="allowExtraEmails" color="primary" />} label="I want to receive inspiration, marketing promotions and updates via email." required />
      </Grid>
     </Grid>
     <Button type="submit" variant="contained" color="primary" className={css.submit} fullWidth>
      Sign Up
     </Button>
     <Grid container justify="flex-end">
      <Grid item>
       <Link
        variant="body2"
        onClick={() => {
         history.push("/SignIn");
        }}
        style={{ cursor: "pointer" }}>
        Already have an account? Sign in
       </Link>
      </Grid>
     </Grid>
    </form>
   </div>
  </Container>
 );
};

export default SignUp;
