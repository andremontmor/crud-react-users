import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { db } from "../firebase/Setting";
import { UpdateUser as Update } from "../data/CRUD";
import { Avatar, Button, CssBaseline, TextField, FormControlLabel, Grid, Typography, Container, Checkbox } from "@material-ui/core";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
/* import { ValidatorForm, TextValidator } from "react-material-ui-form-validator"; */
import useStyles from "../styles/css";

const UpdateUser = () => {
 const css = useStyles();
 let history = useHistory();
 const [user, setUser] = useState([]);

 useEffect(() => {
  /* GET USER */
  db
   .collection("users")
   .doc("wllXFUN8CCcZp7kjyqxU")
   .get()
   .then((doc) => {
    if (doc.exists) {
     console.log(doc.data());
     setUser(doc.data());
    } else {
     setUser("");
    }
   });
 }, []);

 const handleSubmit = (event) => {
  event.preventDefault();
  console.log(user);
  Update(user);
 };

 return (
  <Container component="main" maxWidth="xs">
   <CssBaseline />
   <div className={css.paper}>
    <Avatar className={css.avatar}>
     <LockOutlinedIcon />
    </Avatar>
    <Typography component="h1" variant="h5">
     Update User
    </Typography>
    <form className={css.form} onSubmit={handleSubmit}>
     <Grid container spacing={2}>
      <Grid item xs={12}>
       <TextField
        key={`${Math.floor(Math.random() * 1000)}-min`}
        id="signUpUserName"
        name="userName"
        label="User Name"
        autoComplete="username"
        defaultValue={user.username}
        variant="outlined"
        onChange={(event) => {
         setUser((state) => ({
          ...state,
          username: event.target.value,
         }));
        }}
        fullWidth
        required
        autoFocus
       />
      </Grid>
      <Grid item xs={12} sm={6}>
       <TextField
        key={`${Math.floor(Math.random() * 1000)}-min`}
        id="signUpName"
        name="firstName"
        label="First Name"
        autoComplete="First name"
        defaultValue={user.name}
        variant="outlined"
        onChange={(event) => {
         setUser((state) => ({
          ...state,
          name: event.target.value,
         }));
        }}
        fullWidth
        required
       />
      </Grid>
      <Grid item xs={12} sm={6}>
       <TextField
        key={`${Math.floor(Math.random() * 1000)}-min`}
        id="signUpLastName"
        name="lastName"
        label="Last Name"
        autoComplete="lastname"
        defaultValue={user.lastName}
        variant="outlined"
        onChange={(event) => {
         setUser((state) => ({
          ...state,
          lastName: event.target.value,
         }));
        }}
        fullWidth
        required
       />
      </Grid>
      <Grid item xs={12}>
       <TextField
        key={`${Math.floor(Math.random() * 1000)}-min`}
        id="signUpEmail"
        name="email"
        label="Email Address"
        autoComplete="email"
        defaultValue={user.email}
        variant="outlined"
        onChange={(event) => {
         setUser((state) => ({
          ...state,
          email: event.target.value,
         }));
        }}
        type="email"
        fullWidth
       />
      </Grid>
      <Grid item xs={12}>
       <TextField
        key={`${Math.floor(Math.random() * 1000)}-min`}
        id="signUpPassword"
        name="password"
        label="Password"
        autoComplete="password"
        defaultValue={user.password}
        variant="outlined"
        type="password"
        onChange={(event) => {
         setUser((state) => ({
          ...state,
          password: event.target.value,
         }));
        }}
        required
        fullWidth
       />
      </Grid>
     </Grid>
     <Button type="submit" variant="contained" color="primary" className={css.submit} fullWidth>
      Update
     </Button>
     <Button
      style={{ background: "red", color: "white" }}
      type="submit"
      variant="contained"
      fullWidth
      onClick={() => {
       history.push("/");
      }}>
      Cancel
     </Button>
    </form>
   </div>
  </Container>
 );
};

export default UpdateUser;
