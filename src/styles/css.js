import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
 paper: {
  marginTop: theme.spacing(3),
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
 },
 avatar: {
  margin: theme.spacing(1),
  backgroundColor: theme.palette.primary.main,
 },
 form: {
  width: "100%",
  marginTop: theme.spacing(1),
 },
 submit: {
  margin: theme.spacing(3, 0, 2),
 },
 listUsers: { width: "100%" },
 icons: { margin: "" },
 Fab: { margin: 10 },
}));

export default useStyles;
