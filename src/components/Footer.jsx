import { Box, Fab } from "@material-ui/core";
import { LinkedIn, Email, Copyright } from "@material-ui/icons";
import useStyles from "../styles/css";

const Footer = () => {
 const css = useStyles();
 return (
  <Box display="flex" justifyContent="center" margin="10px">
   <Fab style={{ background: "#0077b5", color: "white" }} href="https://linkedin.com/in/webgc/" target="_black" className={css.Fab}>
    <LinkedIn fontSize="large" />
   </Fab>

   <Fab style={{ background: "red", color: "white" }} className={css.Fab} href="https://mailto:webgc7@gmail.com" target="_black">
    <Email fontSize="large" />
   </Fab>

   <Fab style={{ background: "#61DAFB", color: "white" }} className={css.Fab}>
    <Copyright fontSize="large" />
   </Fab>
  </Box>
 );
};

export default Footer;
