import { BrowserRouter, Switch, Route } from "react-router-dom";
import Header from "./Header";
import Footer from "./Footer";
import Users from "../views/Users";
import SignUp from "../views/SignUp";
import SignIn from "../views/SignIn";
import UpdateUser from "../views/UpdateUser";

const Router = () => {
 return (
  <BrowserRouter>
   <Header />
   <Switch>
    <Route exact path="/" component={Users} />
    <Route exact path="/Home" component={Users} />
    <Route exact path="/SignUp" component={SignUp} />
    <Route exact path="/SignIn" component={SignIn} />
    <Route exact path="/UpdateUser" component={UpdateUser} />
   </Switch>
   <Footer />
  </BrowserRouter>
 );
};

export default Router;
