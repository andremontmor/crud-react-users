import React, { useState, useEffect } from "react";
import { useHistory, useLocation } from "react-router-dom";
import { signOut, hookSetStateAuth } from "../firebase/Authentication";
import { BottomNavigation, BottomNavigationAction, Box } from "@material-ui/core";
import { Home, PersonAdd, Lock, Notifications, ExitToApp } from "@material-ui/icons";
import logo from "../images/logo.svg";

const HeaderImagen = () => (
 <Box display="flex" flexDirection="row" style={{ background: "black" }}>
  <img src={logo} className="App-logo" alt="logo" draggable="false" />
  <h1
   style={{
    color: "white",
    alignContent: "center",
    userSelect: "none",
   }}>
   CRUD
  </h1>
 </Box>
);

const Header = () => {
 /* value: Position of the current route in the header */
 const [value, setValue] = useState();
 const [stateAuth, setStateAuth] = useState(false);
 let history = useHistory();
 let location = useLocation();

 const currrentRoute = () => {
  switch (location.pathname) {
   case "/":
    setValue(0);
    break;

   case "/Home":
    setValue(0);
    break;

   case "/SignUp":
    setValue(1);
    break;

   case "/SignIn":
    setValue(2);
    break;

   case "/Notifications":
    setValue(1);
    break;

   default:
    break;
  }
 };

 const PUSH = () => {
  history.push("/");
 };

 useEffect(() => {
  hookSetStateAuth(setStateAuth);
  currrentRoute();
 }, []);

 return stateAuth ? (
  <>
   <HeaderImagen />
   <BottomNavigation
    value={value}
    onChange={(event, newValue) => {
     setValue(newValue);
    }}
    showLabels
    style={{ background: "white" }}>
    <BottomNavigationAction
     label="Home"
     icon={<Home />}
     onClick={() => {
      history.push("/Home");
     }}
    />
    <BottomNavigationAction
     label="Notifications"
     icon={<Notifications />}
     onClick={() => {
      history.push("/Notifications");
     }}
    />
    <BottomNavigationAction
     label="Sign Out"
     icon={<ExitToApp />}
     onClick={() => {
      setValue(0);
      signOut(PUSH);
     }}
    />
   </BottomNavigation>
  </>
 ) : (
  <>
   <HeaderImagen />
   <BottomNavigation
    value={value}
    onChange={(event, newValue) => {
     setValue(newValue);
    }}
    showLabels
    style={{ background: "white" }}>
    <BottomNavigationAction
     label="Home"
     icon={<Home />}
     onClick={() => {
      history.push("/Home");
     }}
    />
    <BottomNavigationAction
     label="Sign Up"
     icon={<PersonAdd />}
     onClick={() => {
      history.push("/SignUp");
     }}
    />
    <BottomNavigationAction
     label="Sign In"
     icon={<Lock />}
     onClick={() => {
      history.push("/SignIn");
     }}
    />
   </BottomNavigation>
  </>
 );
};

export default Header;
