import React, { useState } from "react";
import { IconButton } from "@material-ui/core";
import { Close } from "@material-ui/icons";
import { Alert as ALERT } from "@material-ui/lab";

const Alert = (props) => {
 const [open, setOpen] = useState(true);

 return (
  <ALERT
   variant="filled"
   severity="error"
   action={
    <IconButton
     aria-label="close"
     color="inherit"
     size="small"
     onClick={() => {
      props.state(false);
     }}>
     <Close fontSize="inherit" />
    </IconButton>
   }>
   {props.messageError}
  </ALERT>
 );
};

export default Alert;
