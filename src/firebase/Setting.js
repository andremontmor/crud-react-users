import firebase from "firebase/app";
import "firebase/firestore";
require("firebase/auth");

const firebaseConfig = {
 apiKey: "AIzaSyBPdMfnAA_Bw0D7y9IGuqDQqQjgIYvpQ8s",
 authDomain: "crud-react-users.firebaseapp.com",
 projectId: "crud-react-users",
 storageBucket: "crud-react-users.appspot.com",
 messagingSenderId: "274244056929",
 appId: "1:274244056929:web:d9b99f7277c7464a8d7720",
};

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();
export const db = firebase.firestore();
