import { auth } from "./Setting";

export const signIn = (pushHome, setAlertErrorAuth, setMessageError) => {
 var email = document.getElementById("signInEmail").value;
 var password = document.getElementById("signInPassword").value;
 auth
  .signInWithEmailAndPassword(email, password)
  .then((userCredential) => {
   console.log("Sign In ", userCredential.user.email);
   pushHome();
  })
  .catch((error) => {
   setMessageError(error.message);
   setAlertErrorAuth(true);
  });
};

export const signOut = () => {
 auth
  .signOut()
  .then(() => {
   console.log("Sign Out");
  })
  .catch((error) => {
   console.log(error);
  });
};

export const hookSetStateAuth = (setState) => {
 auth.onAuthStateChanged((user) => {
  if (user) {
   setState(true);
  } else {
   setState(false);
  }
 });
};

export const getUser = () => {
 return auth.currentUser;
};
